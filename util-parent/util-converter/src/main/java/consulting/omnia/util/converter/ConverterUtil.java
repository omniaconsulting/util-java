package consulting.omnia.util.converter;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import consulting.omnia.util.cast.CastUtil;
import consulting.omnia.util.classloader.ClassLoaderScanner;
import consulting.omnia.util.reflection.ReflectionUtil;

/**
 * Converter util class.
 * It scans the classpath to find any
 * <code>Converter</code> types. 
 * It tries to match the best converter to
 * apply, given the type of the input data to
 * be converted and the desired output type.
 *
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Aug 30, 2015
 * @version 1.0.0
 * 
 * @see Converter
 */
@SuppressWarnings("rawtypes")
public final class ConverterUtil {
	
	private static final int FROM = 0;
	private static final int TO = 1;
	private static final Map<Class, Map<Class, Converter>> converters = new HashMap<Class, Map<Class, Converter>>();

	static {
		try {
			final ClassLoaderScanner scanner = new ClassLoaderScanner();
			final List<Class<Converter>> klasses = scanner.scan(Converter.class);
			for (final Class<Converter> klass : klasses) {
				final Class<?>[] types = listTypes(klass);
				if (!converters.containsKey(types[FROM])) {
					converters.put(types[FROM], new HashMap<Class, Converter>());
				}
				converters.get(types[FROM]).put(types[TO], klass.newInstance());
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Converts the <code>value</code> from its own type <code>T</code> to
	 * <code>convertToType</code> type <code>R</code> using a
	 * <code>Converter</code> if it could find one.
	 * 
	 * @param <T>
	 *            convertFromType
	 * @param <R>
	 *            convertToType
	 * @param value
	 *            data to convert
	 * @param convertToType
	 *            type to convert to
	 * @return Converted value as convertToType
	 */
	@SuppressWarnings("unchecked")
	public static <T, R> R convertData(final T value, final Class<R> convertToType) {
		try {
			final Converter<T, R> converter = (Converter<T, R>) findConverter(value.getClass(), convertToType);
			final R result = converter.format((T) value);
			return convertToType.cast(result);
		} catch (IllegalStateException e) {
			try {
				return CastUtil.castAs(value, convertToType);
			} catch (RuntimeException re) {
				throw new IllegalStateException("Error converting value: [" + value + "] to type: [" + convertToType.getCanonicalName() + "]", e);
			}
		}
	}

	/**
	 * Restores (by converting) the data from its own type <code>R</code> to
	 * <code>restoreToType</code> type <code>T</code> using a
	 * <code>Converter</code> if it could find one.
	 * 
	 * @param <T>
	 *            convertFromType
	 * @param <R>
	 *            convertToType
	 * @param value
	 *            data to restore
	 * @param restoreToType
	 *            type to restore to
	 * @return Converted value as restoreToType
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public static <T, R> T restoreData(final R value, final Class<T> restoreToType) throws ParseException {
		try{
			final Converter<T, R> converter = (Converter<T, R>) findConverter(restoreToType, value.getClass());
			final T result = converter.parse((R) value);
			return restoreToType.cast(result);
		} catch (IllegalStateException e) {
			try {
				return CastUtil.castAs(value, restoreToType);
			} catch (RuntimeException re) {
				throw new IllegalStateException("Error converting value: [" + value + "] to type: [" + restoreToType.getCanonicalName() + "]", e);
			}
		}
	}

	private static Class<?>[] listTypes(final Class<Converter> klass) {
		final Class<?>[] result = ReflectionUtil.listGenericTypes(klass);
		return result;
	}

	@SuppressWarnings("unchecked")
	private static <T, R> Converter<T, R> findConverter(final Class<T> fromType, final Class<R> toType) {
		if (converters.containsKey(fromType)) {
			final Map<Class, Converter> map = converters.get(fromType);
			if (map != null && map.containsKey(toType)) {
				final Converter result = map.get(toType);
				if (result != null) {
					return result;
				}
			}
		}

		throw new IllegalStateException(
				"Unable to find a suitable converter in classpath. Types:[from: " + fromType + ", to: " + toType);
	}
}