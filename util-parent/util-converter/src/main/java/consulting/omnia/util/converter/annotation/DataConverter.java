package consulting.omnia.util.converter.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import consulting.omnia.util.converter.Converter;

/**
 * Defines the converter for a particular field.
 *
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Aug 30, 2015
 * @version 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@SuppressWarnings("rawtypes")
public @interface DataConverter {
	
	Class<? extends Converter> converter();
	Class<?> inputType();
	Class<?> outputType();
}
