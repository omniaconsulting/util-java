package consulting.omnia.util.converter;

import java.text.ParseException;

/**
 * Converter contract.
 * 
 * @param T src object type
 * @param R dst object type
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Aug 30, 2015
 * @version 1.0.0
 */
public interface Converter<T, R> {

	public R format(final T value);

	public T parse(final R value) throws ParseException;
}