package consulting.omnia.util.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ReflectionUtil {

	public static Set<Field> listAllFields(final Object obj, final Comparator<Field> comparator) {
		final Class<?> klass = obj.getClass();
		return listAllFields(klass, comparator);
	}
	
	@SuppressWarnings("rawtypes")
	public static Set<Field> listAllFields(final Class klass, final Comparator<Field> comparator) {
	    final Set<Field> result = new TreeSet<Field>(comparator);	    

	    Class clazz = klass;
	    while (clazz != null) {
	    	for (final Field field : Arrays.asList(clazz.getDeclaredFields())) {
	    		result.add(field);
			}
	        clazz = clazz .getSuperclass();
	    }
	    
	    return result;
	}
	
	
	public static Class<?>[] listGenericTypes(final Class<?> klass) {
		final List<Class<?>> types = new ArrayList<Class<?>>();
		
		for (final Type _parameterizedType : klass.getGenericInterfaces()) {
			System.out.println(_parameterizedType);
			if(_parameterizedType instanceof ParameterizedType) {
				final ParameterizedType parameterizedType = (ParameterizedType) _parameterizedType;
				for (final Type type : parameterizedType.getActualTypeArguments()) {
					if(type instanceof Class) {
						Class<?> classType = (Class<?>) type;
						types.add(classType);
					}
				}
			}
		}
		
		final Class<?>[] result = new Class<?>[types.size()];
		int index = 0;
		for (final Class<?> clazz : types) {
			result[index++] = clazz;
		}
		
		return result;
	}
}
