package consulting.omnia.util.cast;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

public class CastUtilTest {

	@Test
	public void integerToByteOk() throws ParseException {
		final Object _int = new Integer(2);
		final Object _byte = CastUtil.castAs(_int, Byte.class);
		assertEquals(Byte.class, _byte.getClass());
		assertEquals(new Byte((byte) 2), _byte);
	}

	@Test
	public void booleanToLongOk() throws ParseException {
		final Object bool = Boolean.TRUE;
		final Object _long = CastUtil.castAs(bool, Long.class);
		assertEquals(Long.class, _long.getClass());
		assertEquals(1L, _long);
	}

	
	@Test
	public void dateToLongOk() throws ParseException {
		final Object date = new Date();
		final Object _long = CastUtil.castAs(date, Long.class);
		assertEquals(Long.class, _long.getClass());
		assertEquals(((Date)date).getTime(), _long);
	}


	@Test
	public void dateToStringOk() throws ParseException {
		final Object date = new Date();
		final Object string = CastUtil.castAs(date, String.class);
		assertEquals(String.class, string.getClass());
		assertEquals(String.valueOf(((Date)date).getTime()), string);
	}

	@Test
	public void numberStringToDateOk() throws ParseException {
		final Object object = "1438452597017";
		final Object date = CastUtil.castAs(object, Date.class);
		assertEquals(Date.class, date.getClass());
		assertEquals(new Date(1438452597017L), date);
	}
	
	@Test
	public void stringToDateOk() throws ParseException {
		final Object object = "2015-08-01";
		final Object date = CastUtil.castAs(object, Date.class);
		assertEquals(Date.class, date.getClass());
		assertEquals(new Date(1438398000000L), date);		
	}

	@Test
	public void stringToDateHourOk() throws ParseException {
		final Object string = "2015-08-01 15:09:57";
		final Object date = CastUtil.castAs(string, Date.class);
		assertEquals(Date.class, date.getClass());
		assertEquals(new Date(1438452597000L), date);		
	}

	@Test
	public void stringToDateHourMilisOk() throws ParseException {
		final Object string = "2015-08-01 15:09:57.017";
		final Object date = CastUtil.castAs(string, Date.class);
		assertEquals(Date.class, date.getClass());
		assertEquals(new Date(1438452597017L), date);		
	}
	
	@Test(expected=RuntimeException.class)
	public void stringToDateNOk() throws ParseException {
		final Object string = "";
		CastUtil.castAs(string, Date.class);
	}

	@Test(expected=RuntimeException.class)
	public void nullToStringNOk() throws ParseException {
		final Object _null = null;
		CastUtil.castAs(_null, String.class);
	}

	@Test
	public void stringToFloatOk() throws ParseException {
		final Object string = new String("3.1415");
		final Object _float = CastUtil.castAs(string, Float.class);
		assertEquals(Float.class, _float.getClass());
		assertEquals(new Float(3.1415), _float);
	}

	@Test
	public void stringToLongOk() throws ParseException {
		final Object string = new String("3.1415");
		final Object _long = CastUtil.castAs(string, Long.class);
		assertEquals(Long.class, _long.getClass());
		assertEquals(new Long(3L), _long);
	}

	@Test
	public void charToLongOk() throws ParseException {
		final Object _char = new Character('3');
		final Object _long = CastUtil.castAs(_char, Long.class);
		assertEquals(Long.class, _long.getClass());
		assertEquals(new Long(3L), _long);
	}
		
	@Test
	public void booleanFromCharOk() {
		assertTrue(CastUtil.castAs('T', Boolean.class));
		assertFalse(CastUtil.castAs('F', Boolean.class));
		assertTrue(CastUtil.castAs('V', Boolean.class));
		assertFalse(CastUtil.castAs('F', Boolean.class));
		assertTrue(CastUtil.castAs('R', Boolean.class));
		assertFalse(CastUtil.castAs('F', Boolean.class));
	}
	
	@Test
	public void booleanFromCharNOk() {
		assertFalse(CastUtil.castAs('P', Boolean.class));
	}
	
	@Test
	public void stringFromStringOk() throws ParseException {
		final String value = CastUtil.castAs("TEST", String.class);
		assertEquals(String.class, value.getClass());
	}

}
