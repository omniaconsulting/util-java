package consulting.omnia.util.cast;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

import consulting.omnia.util.cast.number.NumberCast;

public class NumberCastTest {

	@Test
	public void testA() {
		final Long long1 = NumberCast.asNumber("2", Long.class);
		assertEquals(2L, long1.longValue());
	}
	
	@Test
	public void testB() {
		final Long long1 = NumberCast.asNumber(3.14F, Long.class);
		assertEquals(3L, long1.longValue());
	}
	
	@Test
	public void testC() {
		final Long long1 = NumberCast.asNumber((byte)4, Long.class);
		assertEquals(4L, long1.longValue());
	}

	@Test
	public void testD() {
		final Long long1 = NumberCast.asNumber(Boolean.TRUE, Long.class);
		assertEquals(1L, long1.longValue());
	}

	@Test
	public void testE() {
		final Date date = new Date();
		final Long number = NumberCast.asNumber(date, Long.class);
		assertEquals(date.getTime(), number.longValue());

	}

	@Test
	public void testF() {
		final Long long21 = 21L;
		final String number = NumberCast.asType(long21, String.class);
		assertEquals("21", number);

	}

	@Test
	public void testG() {
		final Long long21 = 21L;
		final Boolean number = NumberCast.asType(long21, Boolean.class);
		assertEquals(Boolean.FALSE, number);

	}

	@Test
	public void testH() {
		final Long sometime = 1442640114116L;
		final Date number = NumberCast.asType(sometime, Date.class);
		assertEquals(new Date(1442640114116L), number);
	}

}
