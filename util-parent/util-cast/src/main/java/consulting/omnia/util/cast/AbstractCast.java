package consulting.omnia.util.cast;

/** 
* @author Ronaldo Blanc ronaldoblanc at omnia.consulting
* @since Jul 17, 2015
* @version 1.2.0
*/
public abstract class AbstractCast {

	/**
	 * Simple casts the value as type.
	 * If the object is suitable, so no conversion.
	 * 
	 * @param value object to cast
	 * @param type class to cast object as
	 * @param <R> returning type
	 * @return if possible returns <code>value</code> cast as class <code>type</code>
	 */
	protected static <R> R castAs(final Object value, final Class<R> type) {
		return type.cast(value);
	}

}
