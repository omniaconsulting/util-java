package consulting.omnia.util.cast.collections;

import java.util.Collections;
import java.util.List;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Sep 19, 2015
 * @version 1.2.0
 */
@SuppressWarnings("unchecked")
public class ListCastUtil {

	
	public static <R> List<R> cast(final List<?> list) {
		if (list != null) {
			try {
				final List<R> result = newInstance(list.getClass());

				for (Object obj : list) {
					result.add((R) obj);
				}

				return result;
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return Collections.emptyList();
	}

	@SuppressWarnings("rawtypes")
	private static <R> List<R> newInstance(Class<? extends List> type) throws InstantiationException, IllegalAccessException {
		return type.newInstance();
	}
}
