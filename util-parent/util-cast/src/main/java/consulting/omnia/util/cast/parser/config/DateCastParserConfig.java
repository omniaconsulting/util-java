package consulting.omnia.util.cast.parser.config;

import java.util.Arrays;
import java.util.List;

/**
 * Configuration for <code>DateCastParser</code> class.
 * It holds format templates, separators (date, time, etc...) and <code>DateCastParserType</code> enum,
 * that defines which comes first days or months.
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Sep 27, 2015
 * @version 1.0.0
 * 
 * @see DateCastParserType
 */
public class DateCastParserConfig {
	
	private static final List<String> yearFirstFormats = Arrays
			.asList(new String[] { "yyyy%1$sMM%1$sdd%2$skk%3$smm%3$sss%4$sSSS",
					"yyyy%1$sMM%1$sdd%2$skk%3$smm%3$sss",
					"yyyy%1$sMM%1$sdd%2$skk%3$smm", "yyyy%1$sMM%1$sdd" });

	private static final List<String> monthFirstFormats = Arrays
			.asList(new String[] { "MM%1$sdd%1$syyyy%2$skk%3$smm%3$sss%4$sSSS",
					"MM%1$sdd%1$syyyy%2$skk%3$smm%3$sss",
					"MM%1$sdd%1$syyyy%2$skk%3$smm", "MM%1$sdd%1$syyyy" });

	private static final List<String> dayFirstFormats = Arrays
			.asList(new String[] { "dd%1$sMM%1$syyyy%2$skk%3$smm%3$sss%4$sSSS",
					"dd%1$sMM%1$syyyy%2$skk%3$smm%3$sss",
					"dd%1$sMM%1$syyyy%2$skk%3$smm", "dd%1$sMM%1$syyyy" });
	
	private static final List<String> timeOnlyFormats = Arrays
			.asList(new String[]{ "kk%3$smm%3$sss%4$sSSS", "kk%3$smm%3$sss", "kk%3$smm" });
	
	private static final List<String> dateSeparators = Arrays.asList(new String[] { "-", "/", "." });
	private static final List<String> dateTimeSeparators = Arrays.asList(new String[] { " ", "'T'" });
	private static final List<String> timeSeparators = Arrays.asList(new String[] { ":", ".", "" });
	private static final List<String> miliSeparators = Arrays.asList(new String[] { ".", "" });
	private static DateCastParserType dateCastType = DateCastParserType.AUTO;


	public static DateCastParserType getDateCastType() {
		return DateCastParserConfig.dateCastType;
	}

	public static void setDateCastType(final DateCastParserType dateCastType) {
		DateCastParserConfig.dateCastType = dateCastType;
	}

	public static void addDateSeparator(final String dateSeparator) {
		DateCastParserConfig.dateSeparators.add(dateSeparator);
	}
	
	public static void addDateTimeSeparator(final String dateTimeSeparator) {
		DateCastParserConfig.dateTimeSeparators.add(dateTimeSeparator);
	}
	
	public static void addTimeSeparator(final String timeSeparator) {
		DateCastParserConfig.timeSeparators.add(timeSeparator);
	}
	
	public static void addMiliSeparator(final String miliSeparator) {
		DateCastParserConfig.miliSeparators.add(miliSeparator);
	}

	public static void addYearFirstFormats(final String format) {
		DateCastParserConfig.yearFirstFormats.add(format);
	}

	public static void addMonthFirstFormats(final String format) {
		DateCastParserConfig.monthFirstFormats.add(format);
	}

	public static void addDayFirstFormat(final String format) {
		DateCastParserConfig.dayFirstFormats.add(format);
	}

	public static void addTimeOnlyFormat(final String format) {
		DateCastParserConfig.timeOnlyFormats.add(format);
	}
	
	public static List<String> getYearFirstFormats() {
		return yearFirstFormats;
	}

	public static List<String> getMonthFirstFormats() {
		return monthFirstFormats;
	}

	public static List<String> getDayFirstFormats() {
		return dayFirstFormats;
	}
	
	public static List<String> getTimeOnlyFormats() {
		return timeOnlyFormats;
	}

	public static List<String> getDateSeparators() {
		return dateSeparators;
	}

	public static List<String> getDateTimeSeparators() {
		return dateTimeSeparators;
	}

	public static List<String> getTimeSeparators() {
		return timeSeparators;
	}

	public static List<String> getMiliSeparators() {
		return miliSeparators;
	}
		
}
