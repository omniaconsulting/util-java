package consulting.omnia.util.cast.parser.config;

public enum DateCastParserType {
	DAY_FIRST, MONTH_FIRST, AUTO;
}

