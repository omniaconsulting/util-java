package consulting.omnia.util.cast.visitor;

import java.util.Date;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Sep 19, 2015
 * @version 1.2.0
 */
public interface MainDataVisitor {
	
	public void visit(Character value);
	public void visit(String value);
	public void visit(Date value);
	public void visit(Boolean value);
	public void visit(Number value);
	public void visit(Class<?> value);
}
