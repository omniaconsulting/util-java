package consulting.omnia.util.cast.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import consulting.omnia.util.cast.AbstractCast;
import consulting.omnia.util.cast.parser.config.DateCastParserConfig;

/**
 * Parses <code>String</code> to <code>Date</code> as defined in <code>DateCastParserConfig</code>. 
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Ago 05, 2015
 * @version 1.2.0
 * 
 * @see DateCastParserConfig
 */
public class DateCastParser extends AbstractCast {
	
	/**
	 * @param value String to cast
	 * @return Date
	 * @throws ParseException
	 */
	public static Date stringToDate(final String value) throws ParseException {
		Throwable e = null;
		boolean auto = false;
		switch (DateCastParserConfig.getDateCastType()) {
		case AUTO:
			auto = true;
			for (final String format : DateCastParserConfig.getYearFirstFormats()) {
				try {
					Date result = tryToParseIt(value, format);
					return result;
				} catch (Throwable t) {
					e = t;
				}
			}
		case MONTH_FIRST:
			for (final String format : DateCastParserConfig.getMonthFirstFormats()) {
				try {
					Date result = tryToParseIt(value, format);
					return result;
				} catch (Throwable t) {
					e = t;
				}
			}
			if(!auto) {
				break;
			}
		case DAY_FIRST:
			for (final String format : DateCastParserConfig.getDayFirstFormats()) {
				try {
					Date result = tryToParseIt(value, format);
					return result;
				} catch (Throwable t) {
					e = t;
				}
			}
		}

		for (final String format : DateCastParserConfig.getTimeOnlyFormats()) {
			try {
				Date result = tryToParseIt(value, format);
				return result;
			} catch (Throwable t) {
				/*Ignored*/
			}
		}

		return handleException(e);
	}

	private static Date tryToParseIt(final String value, final String formatTemplate) throws ParseException {
		Throwable t = null;
		for(final String dateSeparator : DateCastParserConfig.getDateSeparators()) {
			for(final String dateTimeSeparator : DateCastParserConfig.getDateTimeSeparators()) {
				for(final String timeSeparator : DateCastParserConfig.getTimeSeparators()) {
					for(final String miliSeparator : DateCastParserConfig.getMiliSeparators()) {
						try{
							final String format = String.format(formatTemplate, dateSeparator, dateTimeSeparator, timeSeparator, miliSeparator);
							final SimpleDateFormat sdf = new SimpleDateFormat(format);
							final Date result = sdf.parse(value);
							checkResult(result, value, format);
							return result; 
						} catch (Exception e) {
							t = e;
						}	
					}
				}
			}
		}
		return handleException(t);
	}
	
	private static void checkResult(final Date result, final String value, final String format) {
		final SimpleDateFormat sdf = new SimpleDateFormat(format);
		final String aux = sdf.format(result);
		if(!value.equalsIgnoreCase(aux)) {
			throw new IllegalStateException();
		}
	}

	private static <R> R handleException(final Throwable t) throws ParseException {
		if(t == null)  {
			return null;
		}
		
		if(t instanceof ParseException) {
			throw castAs(t, ParseException.class);
		} else {
			throw new RuntimeException(t);
		}
	}
}
