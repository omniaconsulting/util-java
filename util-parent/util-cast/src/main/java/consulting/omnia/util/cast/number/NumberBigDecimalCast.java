package consulting.omnia.util.cast.number;

import java.math.BigDecimal;
import java.math.BigInteger;

import consulting.omnia.util.cast.AbstractCast;
import consulting.omnia.util.cast.CastUtilExceptionHandler;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Ago 05, 2015
 * @version 1.2.0
 */
//TODO improve this class
public abstract class NumberBigDecimalCast extends AbstractCast {
	
	/**
	 * @param value Number to cast
	 * @return A BigDecimal representation of <code>value</code> number.
	 */
	public static BigDecimal numberToBigDecimal(final Number value) {
		final Class<? extends Number> type = value.getClass();
		if (Byte.class.isAssignableFrom(type)) {
			return new BigDecimal(value.byteValue());
		}
		if (Short.class.isAssignableFrom(type)) {
			return new BigDecimal(value.shortValue());
		}
		if (Integer.class.isAssignableFrom(type)) {
			return new BigDecimal(value.intValue());
		}
		if (Long.class.isAssignableFrom(type)) {
			return new BigDecimal(value.longValue());
		}
		if (Double.class.isAssignableFrom(type)) {
			return new BigDecimal(value.doubleValue());
		}
		if (Float.class.isAssignableFrom(type)) {
			return new BigDecimal(value.floatValue());
		}
		if (BigInteger.class.isAssignableFrom(type)) {
			return new BigDecimal(castAs(value, BigInteger.class));
		}
		if (BigDecimal.class.isAssignableFrom(type)) {
			return castAs(value, BigDecimal.class);
		}

		throw CastUtilExceptionHandler.castException(type);
	}

	/**
	 * @param number BigDecimal number to cast
	 * @param type Desired type
	 * @param <T> Desired type
	 * @return A number of type <code>T</code>
	 */
	public static <T> T bigDecimalToNumber(final BigDecimal number, final Class<T> type) {
		if (Byte.class.isAssignableFrom(type)) {
			return type.cast(number.byteValue());
		}
		if (Short.class.isAssignableFrom(type)) {
			return type.cast(number.shortValue());
		}
		if (Integer.class.isAssignableFrom(type)) {
			return type.cast(number.intValue());
		}
		if (Long.class.isAssignableFrom(type)) {
			return type.cast(number.longValue());
		}
		if (Double.class.isAssignableFrom(type)) {
			return type.cast(number.longValue());
		}
		if (Float.class.isAssignableFrom(type)) {
			return type.cast(number.floatValue());
		}
		if (BigInteger.class.isAssignableFrom(type)) {
			return type.cast(new BigInteger(number.toPlainString()));
		}
		if (BigDecimal.class.isAssignableFrom(type)) {
			return type.cast(number);
		}

		throw CastUtilExceptionHandler.castException(type);
	}
	
}
