package consulting.omnia.util.cast.visitor;

import java.util.Date;

import consulting.omnia.util.cast.AbstractCast;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Sep 19, 2015
 * @version 1.2.0
 */
public abstract class AbstractCastMainDataVisitor extends AbstractCast implements MainDataVisitor {
	protected void accept(final Object value) {
		if (value instanceof Character) {
			visit(castAs(value, Character.class));
		}

		if (value instanceof String) {
			visit(castAs(value, String.class));
		}

		if (value instanceof Date) {
			visit(castAs(value, Date.class));
		}

		if (value instanceof Boolean) {
			visit(castAs(value, Boolean.class));
		}

		if (value instanceof Number) {
			visit(castAs(value, Number.class));
		}
		
		if (value instanceof Class<?>) {
			visit(castAs(value, Class.class));
		}
	}
	
}
