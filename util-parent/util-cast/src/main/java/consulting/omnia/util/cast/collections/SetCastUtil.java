package consulting.omnia.util.cast.collections;

import java.util.Collections;
import java.util.Set;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Sep 19, 2015
 * @version 1.2.0
 */
@SuppressWarnings("unchecked")
public class SetCastUtil {

	public static <R> Set<R> cast(final Set<?> set) {
		if (set != null) {
			try {
				final Set<R> result = newInstance(set.getClass());

				for (Object obj : set) {
					result.add((R) obj);
				}

				return result;
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return Collections.emptySet();
	}

	@SuppressWarnings("rawtypes")
	private static <R> Set<R> newInstance(Class<? extends Set> type) throws InstantiationException, IllegalAccessException {
		return type.newInstance();
	}
}
