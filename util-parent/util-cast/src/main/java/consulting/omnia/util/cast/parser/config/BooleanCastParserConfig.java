package consulting.omnia.util.cast.parser.config;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class BooleanCastParserConfig {
	
	// True, Verdadero, Vrai, Verdadeiro, Vero, Richtig
	private static final Map<Locale, String> trueStrings = new HashMap<Locale, String>();
	private static final Map<Locale, Character> trueChars = new HashMap<Locale, Character>();
	// False, Falso, Faux, Falso, Falso, Falsch
	private static final Map<Locale, String> falseStrings = new HashMap<Locale, String>();
	private static final Map<Locale, Character> falseChars = new HashMap<Locale, Character>();
	
	static {
		put(Locale.ENGLISH, "true", 'T', "false", 'F');
		put(new Locale("es"), "verdadero", 'V', "falso", 'F');
		put(Locale.FRENCH, "vrai", 'V', "faux", 'F');
		put(new Locale("pt"), "verdadeiro", 'V', "falso", 'F');
		put(Locale.ITALIAN, "vero", 'V', "falso", 'F');
		put(Locale.GERMAN, "richtig", 'R', "falsch", 'F');
	}

	public static void put(final Locale locale, final String trueValue, final Character trueChar, final String falseValue, final Character falseChar) {
		trueChars.put(locale, trueChar);
		falseChars.put(locale, falseChar);
		trueStrings.put(locale, trueValue);
		falseStrings.put(locale, falseValue);
	}

	public static Map<Locale, String> getTrueStrings() {
		return trueStrings;
	}

	public static Map<Locale, Character> getTrueChars() {
		return trueChars;
	}

	public static Map<Locale, String> getFalseStrings() {
		return falseStrings;
	}

	public static Map<Locale, Character> getFalseChars() {
		return falseChars;
	}
	
}
