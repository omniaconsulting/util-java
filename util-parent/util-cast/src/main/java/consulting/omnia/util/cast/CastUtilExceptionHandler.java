package consulting.omnia.util.cast;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Jul 20, 2015
 * @version 1.2.0
 */
public class CastUtilExceptionHandler {

	public static RuntimeException castException(final Class<?> type) {
		return new UnsupportedOperationException(exceptionMsg(type));
	}

	public static RuntimeException castException(final Object value, final Class<?> type) {
		return new UnsupportedOperationException(exceptionMsg(value, type));
	}

	public static RuntimeException castException(final String message) {
		return new UnsupportedOperationException(message);
	}

	
	private static String exceptionMsg(final Class<?> type) {
		return "Cannot cast to: [" + type.getCanonicalName() + "]";
	}

	
	private static String exceptionMsg(final Object value, final Class<?> type) {
		return "Cannot cast [" + value + "] to: [" + type.getCanonicalName() + "]";
	}

}
