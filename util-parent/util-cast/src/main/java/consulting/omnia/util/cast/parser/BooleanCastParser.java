package consulting.omnia.util.cast.parser;

import java.util.Locale;

import consulting.omnia.util.cast.parser.config.BooleanCastParserConfig;

/**
 *  Parses <code>String</code> to <code>Boolean</code> as defined in <code>BooleanCastParserConfig</code>. 
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Ago 05, 2015
 * @version 1.2.0
 * 
 * @see BooleanCastParserConfig
 */
public class BooleanCastParser {
	
	public static Boolean stringToBoolean(final String value) {
		return BooleanCastParserConfig.getTrueStrings().containsValue(value);	
		
	}
	
	public static Boolean charToBoolean(final Character value) {
		return BooleanCastParserConfig.getTrueChars().containsValue(value);	
	}

	public static String booleanToString(final Boolean value, Locale locale) {
		if(locale == null && !BooleanCastParserConfig.getTrueStrings().containsKey(locale)) {
			locale = Locale.ENGLISH;
		}
		
		return value ? BooleanCastParserConfig.getTrueStrings().get(locale) : BooleanCastParserConfig.getFalseStrings().get(locale);
	}
	
	public static Character booleanToChar(final Boolean value, Locale locale) {
		if(locale == null && !BooleanCastParserConfig.getTrueChars().containsKey(locale)) {
			locale = Locale.ENGLISH;
		}
		
		return value ? BooleanCastParserConfig.getTrueChars().get(locale) : BooleanCastParserConfig.getFalseChars().get(locale);
	}
	
}
