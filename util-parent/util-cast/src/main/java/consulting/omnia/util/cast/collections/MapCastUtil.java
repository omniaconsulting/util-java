package consulting.omnia.util.cast.collections;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Sep 19, 2015
 * @version 1.2.0
 */
@SuppressWarnings("unchecked")
public class MapCastUtil {

	public static <K, V> Map<K, V> cast(final Map<?, ?> map) {
		if (map != null) {
			try {
				final Map<K, V> result = newInstance(map.getClass());

				for (final Entry<?, ?> entry : map.entrySet()) {
					final Object key = entry.getKey();
					final Object value = entry.getValue();
					result.put((K)key, (V)value);
				}

				return result;
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return Collections.emptyMap();
	}

	@SuppressWarnings("rawtypes")
	private static <K, V> Map<K, V> newInstance(Class<? extends Map> type) throws InstantiationException, IllegalAccessException {
		return type.newInstance();
	}
}
