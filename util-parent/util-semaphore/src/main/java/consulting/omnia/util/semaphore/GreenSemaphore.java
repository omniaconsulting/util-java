package consulting.omnia.util.semaphore;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public class GreenSemaphore extends Semaphore {

	public GreenSemaphore() {
		setGreen();
	}
}
