package consulting.omnia.util.semaphore;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public class RedSemaphore extends Semaphore {

	public RedSemaphore() {
		setRed();
	}
}
