package consulting.omnia.util.semaphore;

import consulting.omnia.util.factory.Factory;
import consulting.omnia.util.factory.FactoryUtil;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @see Factory
 * @see FactoryUtil
 */
public enum SemaphoreFactory implements Factory<Semaphore> {

	STOP("Red") {
		public Semaphore newInstance() {
			return new RedSemaphore();
		}
	},
	WAIT("Yellow") {
		public Semaphore newInstance() {
			return new YellowSemaphore();
		}
	},
	GO("Green") {
		public Semaphore newInstance() {
			return new GreenSemaphore();
		}
	};

	private final String state;

	private SemaphoreFactory(final String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}
}
