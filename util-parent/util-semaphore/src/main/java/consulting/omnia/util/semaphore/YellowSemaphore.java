package consulting.omnia.util.semaphore;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public class YellowSemaphore extends Semaphore {

	public YellowSemaphore() {
		setYellow();
	}
}
