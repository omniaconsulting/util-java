package consulting.omnia.util.io.file.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Reads a delimited data record using the <code>clazz</code> parameter
 * <code>Enum</code> as the record fields definition.
 * The resulting <code>Map</code> has the <code>Enum</code> field name as the
 * key and the correlated data on the input file as the value.
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public interface DataReader {

	/**
	 * Reads the data as a <code>java.util.List</code> of
	 * <code>java.util.Map</code> representing all the records in the file.
	 * 
	 * @param is the input stream
	 * @param clazz Enum with record description
	 * @param delimiter the field delimiter character
	 * @param hasHeader header indicator flag
	 * @return A list of records represented by a <code>Map</code>.
	 * @throws IOException if something went wrong
	 */
	@SuppressWarnings("rawtypes")
	public abstract List<Map<String, String>> getDataAsMap(InputStream is,
			Class<? extends Enum> clazz, String delimiter, boolean hasHeader)
			throws IOException;

}
