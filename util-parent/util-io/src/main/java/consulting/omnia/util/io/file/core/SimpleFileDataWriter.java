package consulting.omnia.util.io.file.core;

import java.io.IOException;
import java.util.logging.Logger;

import consulting.omnia.util.io.file.api.NoSuchFileException;

/**
 * Writes out data on the desired file.
 * Very useful to write data on a new file.
 * The <code>init(filename)</code> method creates a file named
 * <code>filename</code> and opens an output stream to it.
 * The <code>writeData(data)</code> method writes out data on the opened file.
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public class SimpleFileDataWriter {
	private static DefaultFileDataWritter fileDataWritter;
	private static final Logger LOGGER = Logger.getLogger(SimpleFileDataWriter.class.getCanonicalName());

	/**
	 * Opens a file for write.
	 * 
	 * @param filename the filename of the file to write data to
	 * @throws NoSuchFileException if something went wrong
	 */
	public static void init(final String filename) throws NoSuchFileException {
		LOGGER.fine(SimpleFileDataWriter.class.getCanonicalName() + " initialized");
		fileDataWritter = new DefaultFileDataWritter(filename);
	}

	/**
	 * Writes data on file.
	 * 
	 * @param data data to write to
	 * @throws IOException if something went wrong
	 */
	public static void write(final String data) throws IOException {
		LOGGER.finest("Output data: [" + data + "]");
		fileDataWritter.write(data);
	}
}
