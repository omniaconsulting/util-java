package consulting.omnia.util.io.file.api;

/**
 * The filename could not be found or access is denied.
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public class NoSuchFileException extends Exception {

	private static final long serialVersionUID = 6072111253299035120L;

	/**
	 * @param filename the wrong file path
	 */
	public NoSuchFileException(String filename) {
		super(filename);
	}

	/**
	 * @param filename the wrong file path
	 * @param cause the cause
	 */
	public NoSuchFileException(String filename, Throwable cause) {
		super(filename, cause);
	}
}
