package consulting.omnia.util.io.file.record;

public enum RecordType {

	HEADER, DETAIL, DETAIL_ONLY, DETAIL_HEADER, DETAIL_TRAILLER, TRAILLER;
}
