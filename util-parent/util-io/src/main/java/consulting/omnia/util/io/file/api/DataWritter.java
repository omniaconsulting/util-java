package consulting.omnia.util.io.file.api;

import java.io.IOException;

/**
 * Writes out data.
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public interface DataWritter {

	/**
	 * Writes the <code>data</code> to the destination.
	 * 
	 * @param data data to write on file
	 * @throws IOException if something went wrong
	 */
	public void write(String data) throws IOException;
}
