package consulting.omnia.util.io.file.api;

import java.io.IOException;

/**
 * Writes data on multiple destinations.
 * Very useful in loops that generates data for different outputs.
 * You must call <code>newWritter()</code> method, before writes data out.
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public interface MultiDataWriter {

	/**
	 * Defines a new writer.
	 * 
	 * @param outputName the name of the file to write to
	 * @throws NoSuchFileException if the file path is wrong
	 */
	public abstract void newWriter(final String outputName) throws NoSuchFileException;

	/**
	 * Checks if a writer exists.
	 * 
	 * @param outputName the name of the file to write to
	 * @return true if the filename writer exists, false otherwise
	 */
	public abstract boolean hasWriter(final String outputName);

	/**
	 * Writes out data.
	 * 
	 * @param outputName
	 *            The destination to writes on.
	 * @param data
	 *            The data that should be written.
	 * @param noDups
	 *            If <code>true</code> duplicate records are not allowed.
	 * @throws NoSuchFileException if the file path is wrong
	 * @throws IOException is something went wrong
	 */
	public abstract void write(final String outputName, final String data, final boolean noDups)
			throws NoSuchFileException, IOException;

}
