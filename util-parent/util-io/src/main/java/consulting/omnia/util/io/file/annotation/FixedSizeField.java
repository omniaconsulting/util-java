package consulting.omnia.util.io.file.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import consulting.omnia.util.format.Padding;

/**
 * Fixed size field info.
 *
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @since Aug 30, 2015
 * @version 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FixedSizeField {

	/**
	 * Field start index on record.
	 * Default 0
	 * @return the index
	 */
	int pos() default 0;

	/**
	 * Field total length
	 * Default 0
	 * @return the index
	 */
	int length() default 0;

	/**
	 * The character to pad to the length if the value's length is shorter than
	 * <code>length()</code>.
	 * Default <code>space</code>
	 * @return the filler
	 */
	String filler() default " ";

	/**
	 * Padding type. LEFT, RIGHT or AUTO
	 * Default AUTO
	 * @return the padding type
	 */
	Padding padding() default Padding.AUTO;

}
