package consulting.omnia.util.io.file;

public enum LineFeedType {

	WINDOWS_LIKE, UNIX_LIKE
}
