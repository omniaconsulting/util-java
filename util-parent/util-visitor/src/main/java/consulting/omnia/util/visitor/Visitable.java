package consulting.omnia.util.visitor;

public interface Visitable {

	/**
	 * @return Returns <code>true</code> if it should visit more results,
	 *         <code>false</code> otherwise.
	 */
	public boolean accept(final Visitor visitor);
}
