package consulting.omnia.util.factory;

/**
 * Utilitary class to help avoiding javac compile bug on <code>Enum</code> factories.
 * <a href="http://bugs.sun.com/view_bug.do?bug_id=6724345">http://bugs.sun.com/
 * view_bug.do?bug_id=6724345</a>
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 * @see Factory
 * @see Enum
 */
public final class FactoryUtil {

	private FactoryUtil() {
	}

	public static <R> R getInstance(Factory<R> factory) throws Exception {
		return factory.newInstance();
	}
}
