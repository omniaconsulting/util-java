package consulting.omnia.util.factory;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public interface Factory<T> {

	T newInstance();
}
