package consulting.omnia.util.format;

/**
 * Padding type.
 * 
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public enum Padding {

	LEFT, RIGHT, AUTO
	
}
