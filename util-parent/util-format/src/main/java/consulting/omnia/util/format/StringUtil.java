package consulting.omnia.util.format;

import java.nio.charset.Charset;
import java.text.Normalizer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StringUtil {

	private static final String LEFT_SPACE_PADDING = "%{}s";
	private static final String RIGHT_SPACE_PADDING = "%-{}s";
	private static final String LEFT_ZERO_PADDING = "%0{}d";
	public static final String UTF8 = "UTF-8";
	public static final String ISO_8859_1 = "ISO-8859-1";
	public static final String WINDOWS_1252 = "windows-1252";

	public static String padding(final Object object, final int size, final String filler, final Padding padding) {
		boolean isBlankFiler = " ".equals(filler);
		String pattern;
		if (object instanceof Number) {
			pattern = getPattern(LEFT_ZERO_PADDING, size);
		} else {
			switch (padding) {
			case LEFT:
				pattern = getPattern(LEFT_SPACE_PADDING, size);
				break;
			case AUTO:
			case RIGHT:
			default:
				pattern = getPattern(RIGHT_SPACE_PADDING, size);
				break;
			}
		}

		Object value = filler;
		if (object != null) {
			value = object;
		}

		String result = String.format(pattern, value);

		if (result.length() > size) {
			result = result.substring(0, size);
		}

		if (!isBlankFiler) {
			result = result.replaceAll(" ", filler);
		}

		return result;
	}

	private static String getPattern(final String patternTemplate, final int size) {
		return patternTemplate.replaceFirst("\\{\\}", String.valueOf(size));
	}

	public static <T> T numbers(final String data, final Class<T> type) {
		if (data == null) {
			return null;
		}

		final String numbers = data.replaceAll("[^\\d]", "");

		if (Long.class.isAssignableFrom(type)) {
			return type.cast(Long.valueOf(numbers));
		}

		if (Integer.class.isAssignableFrom(type)) {
			return type.cast(Integer.valueOf(numbers));
		}

		throw new UnsupportedOperationException("Cannot convert [" + data + "] to [" + type.getCanonicalName() + "]");
	}

	public static boolean checkEncoding(Object data, String encoding) {
		final String charsetDisplayName = Charset.defaultCharset().displayName();
		if(String.class.isAssignableFrom(data.getClass())) {
			try {
				if(encoding.equalsIgnoreCase(charsetDisplayName)) {
					String dataAsString = (String) data;
					new String(dataAsString.getBytes(), encoding);
					return true;
				}
			} catch (Exception e) {
				System.err.println("Wrong charset [" + data + "] is not encoded as [" + encoding + "]");
			}
		}
		return false;
	}
	
	
	public static void outputMessage(final String string) {
		log.info("-------------------------------------------------------------------\n" 
				+ string
				+ "-------------------------------------------------------------------\n");		
	}

	public static boolean isEmpty(final String value) {
		return value == null ? true : value.isEmpty();
	}
	
	public static String toAsciiOnly(final String value) {
		final String normalized = Normalizer.normalize(value, Normalizer.Form.NFD);
		final String asciiOnly = normalized.replaceAll("[^\\p{ASCII}]", "");
		return asciiOnly;
		}
}
