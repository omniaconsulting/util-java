package consulting.omnia.util.notification;

import consulting.omnia.util.semaphore.*;

/**
 * @author Ronaldo Blanc ronaldoblanc at omnia.consulting
 */
public class SemaphoreNotification extends MessageNotification {

	private final Semaphore semaphore;

	public SemaphoreNotification(final String message, final Semaphore semaphore) {
		super(message);
		this.semaphore = semaphore;
	}

	@Override
	public boolean isSemaphoreStop() {
		return semaphore.isRed();
	}

	@Override
	public boolean isSemaphoreWait() {
		return semaphore.isYellow();
	}

	@Override
	public boolean isSemaphoreGo() {
		return semaphore.isGreen();
	}

}
