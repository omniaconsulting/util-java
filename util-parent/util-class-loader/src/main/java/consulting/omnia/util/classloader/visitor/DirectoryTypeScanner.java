package consulting.omnia.util.classloader.visitor;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import consulting.omnia.util.classloader.elements.ClassFile;
import consulting.omnia.util.classloader.elements.Directory;
import consulting.omnia.util.classloader.elements.JarPath;
import consulting.omnia.util.classloader.elements.PathUtil;

public class DirectoryTypeScanner implements ClassLoaderVisitor {
	private static final String JAR = ".jar";
	private static final String CLASS = ".class";
	private final List<Class<?>> result;
	private final Class<?> type;
	private final String packageName;
	private String rootDirPath;

	public DirectoryTypeScanner(final Class<?> type, final String packageName) {
		this.type = type;
		this.packageName = packageName;
		this.result = new LinkedList<Class<?>>();
	}

	public List<Class<?>> scan(final Directory rootDir){
		this.rootDirPath = PathUtil.asJavaPath(rootDir.getAbstractPath());
		visit(rootDir);
		return result;
	}
	
	@Override
	public void visit(final Directory value) {
		for (final File abstractPath : value.listFiles()) {
			if (abstractPath.isDirectory()) {
				final Directory directory = new Directory(abstractPath);
				visit(directory);
			} else if (abstractPath.isFile()) {
				visit(abstractPath);
			}
		}
	}
	
	public void visit(final File file) {
		if (file.getAbsolutePath().endsWith(CLASS)) {
			final ClassFile classFile = new ClassFile(file);
			visit(classFile);
		}

		if (file.getAbsolutePath().endsWith(JAR)) {
			final JarPath jarFile = new JarPath(file);
			visit(jarFile);
		}
	}

	@Override
	public void visit(final ClassFile value) {
		try {
			String classname = PathUtil.asJavaPath(value.getAbstractPath());
			classname = classname.replace(CLASS, "");
			String pathPrefix = this.rootDirPath.replace(this.packageName, "");
			if(!pathPrefix.endsWith("/")) {
				pathPrefix += "/";
			}
			classname = classname.replace(pathPrefix, ""); 
			classname = classname.replace("/", ".");
			final Class<?> klass = Class.forName(classname);
			if (this.type.isAssignableFrom(klass) && !this.type.equals(klass)) {
				result.add(klass);
			}
		} catch (Throwable t) {
			return;
		}
	}

	@Override
	public void visit(final JarPath value) {
		JarFile jar = null;
		try {
			jar = new JarFile(value.getAbstractPath());
			final Enumeration<JarEntry> entries = jar.entries();
			while (entries.hasMoreElements()) {
				final JarEntry jarEntry = entries.nextElement();
				final String entryName = jarEntry.getName();
				if (entryName.endsWith(CLASS)) {
					final ClassFile classFile = new ClassFile(value.getAbstractPath());
					visit(classFile);
				}
			}
		} catch (Throwable e) {
			return;
		} finally {
			if (jar != null) {
				try {
					jar.close();
				} catch (IOException e) {
					/* Ignore */
				}
			}
		}
	}

}
