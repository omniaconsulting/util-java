package consulting.omnia.util.classloader.elements;

import java.io.File;

public final class PathUtil {

	public static String asJavaPath(final File file) {
		if (file != null) {
			return asJavaPath(file.getAbsolutePath());
		} else {
			throw new IllegalStateException("file cannot be null");
		}
	}

	public static String asJavaPath(final String filePath) {
		if (filePath != null && !filePath.isEmpty()) {
			return filePath.replace("\\", "/");
		} else {
			throw new IllegalStateException("filePath cannot be null");
		}
	}
}
