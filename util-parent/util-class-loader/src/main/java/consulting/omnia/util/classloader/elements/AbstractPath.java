package consulting.omnia.util.classloader.elements;

import java.io.File;

import consulting.omnia.util.visitor.Visitable;

public abstract class AbstractPath implements Visitable{

	private File abstractPath;

	public AbstractPath(final File abstractPath) {
		this.abstractPath = abstractPath;
	}

	public File getAbstractPath() {
		return abstractPath;
	}

	public void setAbstractPath(final File abstractPath) {
		this.abstractPath = abstractPath;
	}

}
