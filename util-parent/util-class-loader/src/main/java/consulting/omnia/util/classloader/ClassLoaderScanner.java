package consulting.omnia.util.classloader;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import consulting.omnia.util.cast.CastUtil;
import consulting.omnia.util.classloader.elements.Directory;
import consulting.omnia.util.classloader.visitor.DirectoryTypeScanner;

public class ClassLoaderScanner {

	private static final String CLASS_EXTENSION = ".class";
	private final String packageName;
	private final List<Class<?>> foundTypes;

	public ClassLoaderScanner(final String packageName) {
		this.packageName = packageName;
		this.foundTypes = new LinkedList<Class<?>>();
	}

	public ClassLoaderScanner() {
		this(null);
	}

	public <R> List<Class<R>> scan(final Class<R> type) throws IOException,
			ClassNotFoundException {
		final ClassLoader contextClassLoader = Thread.currentThread()
				.getContextClassLoader();
		final String _packageName = nullSafePackageName(packageName);
		final Enumeration<URL> resources = contextClassLoader
				.getResources(_packageName);

		while (resources.hasMoreElements()) {
			final URL resource = resources.nextElement();
			final String path = resource.getPath();

			if (path != null && !path.isEmpty()) {
				if ("file".equalsIgnoreCase(resource.getProtocol())) {
					final Directory rootDir = new Directory(new File(path));
					final DirectoryTypeScanner scanner = new DirectoryTypeScanner(
							type, _packageName);
					foundTypes.addAll(scanner.scan(rootDir));
				}

				if (resource.getProtocol() != null
						&& resource.getProtocol().toLowerCase().contains("jar")) {
					foundTypes.addAll(processJarEntries(resource, _packageName,
							type, contextClassLoader));
				}
			}
		}
		return CastUtil.getAsList(foundTypes);
	}

	private <R> List<Class<?>> processJarEntries(final URL resource, final String packageName, final Class<R> type,
			final ClassLoader contextClassLoader) throws IOException,
			ClassNotFoundException {
		final List<Class<?>> result = new LinkedList<Class<?>>();
		final JarURLConnection jarUrlConnection = (JarURLConnection) (resource.openConnection());
		final JarFile jar = jarUrlConnection.getJarFile();
		final Enumeration<JarEntry> entries = jar.entries();

		while (entries.hasMoreElements()) {
			final JarEntry nextElement = entries.nextElement();
			if (nextElement.getName().startsWith(packageName) && nextElement.getName().endsWith(CLASS_EXTENSION)) {
				final String className = nextElement.getName().replace(CLASS_EXTENSION, "").replace("/", ".");
				final Class<?> loadClass = contextClassLoader.loadClass(className);
				if (type.isAssignableFrom(loadClass)) {
					result.add(loadClass);
				}
			}
		}

		return result;
	}

	private String nullSafePackageName(final String packageName) {
		return packageName == null ? "" : packageName.replace(".", "/");
	}
}
