package consulting.omnia.util.classloader.elements;

import java.io.File;

import consulting.omnia.util.visitor.Visitor;

public class Directory extends AbstractPath {

	public Directory(final File abstractPath) {
		super(abstractPath);
		if (!abstractPath.isDirectory()) {
			throw new IllegalStateException("Path [" + abstractPath + "] is not a directory");
		}

	}

	public File[] listFiles() {
		return getAbstractPath().listFiles();
	}
	
	@Override
	public boolean accept(final Visitor visitor) {
		return false;
	}
}
