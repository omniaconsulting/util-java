package consulting.omnia.util.classloader.elements;

import java.io.File;

import consulting.omnia.util.visitor.Visitor;

public class JarPath extends AbstractPath {

	public JarPath(final File abstractPath) {
		super(abstractPath);
	}

	@Override
	public boolean accept(final Visitor visitor) {
		return false;
	}
}
