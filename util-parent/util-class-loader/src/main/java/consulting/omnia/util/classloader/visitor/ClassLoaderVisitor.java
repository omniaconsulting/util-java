package consulting.omnia.util.classloader.visitor;

import java.io.File;

import consulting.omnia.util.classloader.elements.ClassFile;
import consulting.omnia.util.classloader.elements.Directory;
import consulting.omnia.util.classloader.elements.JarPath;

public interface ClassLoaderVisitor {

	public void visit(final Directory directory);
	
	public void visit(final JarPath jarPath);
	
	public void visit(final ClassFile classFile);
	
	public void visit(final File file);
}
